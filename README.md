This repository holds the content and physical output for my personal blog.

The content is maintained in the source branch mostly in the form of markdown posts.
[nanoc](http://nanoc.stoneship.org) is used to compile the website. The compiled
output is maintained in this branch (master) which is deployed to 
[heroku](http://www.heroku.com) with a simple git push.

## License
Feel free to reuse any code from this project (there isn't much).
All other content is licensed under a 
[Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-nc-sa/3.0/)
